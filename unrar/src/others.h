#ifndef OTHER_H
#define OTHER_H 1

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

#ifdef _DEBUG_LOG
#define cli_dbgmsg(fmt,args...) fprintf(stderr,fmt,##args)
#define cli_errmsg cli_dbgmsg
#define cli_warnmsg cli_dbgmsg
#else
#define cli_dbgmsg(fmt,args...) { }
#define cli_errmsg cli_dbgmsg
#define cli_warnmsg cli_dbgmsg
#endif
#define cli_realloc realloc
#define cli_realloc2 realloc
#define cli_readn read
#define cli_writen write
#define cli_malloc malloc
#define cli_strdup strdup

#define le16_to_host(v) (ntohs( ((v)>>8)|((v)<<8) ))
#define le32_to_host(v) (ntohl( ((v)>>24)|(((v)<<8)&0x00ff0000)|(((v)>>8)&0x0000ff00)|((v)<<24) ))
/*
#define le16_to_host(v) (v)
#define le32_to_host(v) (v)
*/

enum CL_ALL {
  CL_SUCCESS=0, CL_ENULLARG=1, CL_EFORMAT=2, CL_EMEM=3, 
  CL_ERAR=4, CL_ETMPDIR=5,
  CL_EIO=6, CL_BREAK=7, CL_EOPEN=8
};

#endif
